# Backup List

This will copy a list of sub-directories from a text file from a specified
source directory to a specified destination directory.

# Why Rust?

Why not Python in fact? I had made this implementation in Python first but my
use of the `shutil.copytree()` method was extraordinarily slow.

Also, I just like writing things in Rust.

# Usage

```
backup_list [sub-directory text file] [source dir] [destination dir]
```

# Example

```
backup_list ./my-list.txt /source/path/to/copy/from /destination/path/to/copy/to
```
