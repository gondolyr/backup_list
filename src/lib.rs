use std::error::Error;
use std::path::Path;
use std::{fs, io};

/// File extensions that should not be copied.
///
/// These are often metadata files or temporary files.
const IGNORED_EXT: [&str; 1] = ["tmp"];

pub struct Config {
    pub directory_list: String,
    pub source_dir: String,
    pub dest_dir: String,
}

impl Config {
    pub fn new(mut args: std::env::Args) -> Result<Config, &'static str> {
        // Skip the program name.
        args.next();

        let directory_list = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a directory list file"),
        };

        let source_dir = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a source directory"),
        };

        let dest_dir = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a destination directory"),
        };

        Ok(Config {
            directory_list,
            source_dir,
            dest_dir,
        })
    }
}

/// Copy a directory to another. This function will copy recursively if the `recursive` flag is set to `true`.
fn copy_dir(from: &Path, to: &Path, recursive: bool) -> io::Result<()> {
    for entry in from.read_dir()? {
        let entry = entry?;
        let path = entry.path();

        if path.is_dir() && recursive {
            let to_dir = to.join(path.file_name().unwrap());
            copy_dir(&path, &to_dir, true)?;
        } else {
            // Create the directory if it doesn't exist.
            if !to.is_dir() {
                fs::create_dir_all(to)?;
            }
            let to_file = to.join(path.file_name().unwrap());

            // Skip copying the file if it exists or should be ignored based on the file extension.
            if to_file.is_file()
                || to_file.extension().is_none()
                || IGNORED_EXT.contains(&to_file.extension().unwrap().to_str().unwrap())
            {
                continue;
            }

            println!(
                "Source: {}\nDestination: {}",
                from.display(),
                to_file.display()
            );

            fs::copy(path.as_os_str(), to_file.as_os_str())?;
        }
    }

    Ok(())
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let source_dir = Path::new(&config.source_dir);
    let dest_dir = Path::new(&config.dest_dir);

    let list_contents = fs::read_to_string(config.directory_list)?;
    let directories: Vec<&str> = list_contents
        .lines()
        .filter(|d| source_dir.join(d).is_dir())
        .collect();

    println!("Copying directories...");
    for dir in directories {
        let source = source_dir.join(dir);
        let dest = dest_dir.join(dir);

        copy_dir(&source, &dest, true)?;
    }
    println!("Done");

    Ok(())
}
